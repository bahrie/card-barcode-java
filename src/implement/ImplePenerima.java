/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import crudjava.koneksi.DatabaseUtilities;
import entity.Penerima;
import interfc.InterPenerima;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author samz
 */
public class ImplePenerima implements InterPenerima{

    public Penerima insert(Penerima o) throws SQLException {
        PreparedStatement st=DatabaseUtilities.getConnection().prepareStatement("insert into mahasiswa values(?,?,?)");
//        st.setString(1, o.getNim());
        st.setString(2, o.getNama());
        st.setString(3, o.getAlamat());
        st.executeUpdate();
        return o;
    }

    public void update(Penerima o) throws SQLException {
        PreparedStatement st=DatabaseUtilities.getConnection().prepareStatement("update mahasiswa set nama=?,alamat=? where nim=?");
        
        st.setString(1, o.getNama());
        st.setString(2, o.getAlamat());
//        st.setString(3, o.getNim());
        st.executeUpdate();
    }

    public void delete(String nim) throws SQLException {
        PreparedStatement st=DatabaseUtilities.getConnection().prepareStatement("delete from mahasiswa where nim=?");
        st.setString(1, nim);
        st.executeUpdate();
    }

    public List<Penerima> getAll() throws SQLException {
        Statement st=DatabaseUtilities.getConnection().createStatement();
        ResultSet rs=st.executeQuery("select * from pj");
        List<Penerima>list=new ArrayList<Penerima>();
        while(rs.next()){
            Penerima pnr=new Penerima();
            pnr.setNpj(rs.getString("NPJ"));
            pnr.setNama(rs.getString("nama"));
            pnr.setAlamat(rs.getString("alamat"));
            pnr.setLahir(rs.getString("LAHIR"));
            pnr.setRt(rs.getString("RT"));
            pnr.setRw(rs.getString("RW"));
            list.add(pnr);
        }
        return list;
    }
    
}
