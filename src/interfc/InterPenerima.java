/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfc;

import entity.Penerima;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author samz
 */
public interface InterPenerima {
    
    public Penerima insert(Penerima o) throws SQLException;

    public void update(Penerima o) throws SQLException;

    public void delete(String nim) throws SQLException;

    public List<Penerima> getAll() throws SQLException;
    
}
